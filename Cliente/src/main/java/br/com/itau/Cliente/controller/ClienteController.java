package br.com.itau.Cliente.controller;

import br.com.itau.Cliente.models.Cliente;
import br.com.itau.Cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;


    //INCLUSÃO
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente incluirCliente(@RequestBody @Valid Cliente cliente){
        Cliente objCliente = clienteService.incluirCliente(cliente);

        return objCliente;
    }

    //CONSULTA
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Cliente consultarClientePorId(@PathVariable Integer id){
        Cliente objCliente = clienteService.buscarClientePorId(id);

        return objCliente;
    }
}
