package br.com.itau.Acesso.DTO;

import br.com.itau.Acesso.model.Acesso;
import br.com.itau.Acesso.model.AcessoPK;
import org.springframework.stereotype.Component;

@Component
public class AcessoMapper {

    public Acesso converterParaAcesso(Integer idCliente, Integer idPorta){
        AcessoPK acessoPK = new AcessoPK();
        acessoPK.setClienteId(idCliente);
        acessoPK.setPortaId(idPorta);

        Acesso acesso = new Acesso(acessoPK);

        return acesso;
    }

    public AcessoPK converterParaAcessoPK(Integer idCliente, Integer idPorta){
        AcessoPK acessoPK = new AcessoPK();
        acessoPK.setClienteId(idCliente);
        acessoPK.setPortaId(idPorta);

        return acessoPK;
    }

    public AcessoRequestResponse converterParaAcessoRequestResponse(Acesso acesso){
        AcessoRequestResponse acessoResponse = new AcessoRequestResponse();
        acessoResponse.setClienteId(acesso.getId().getClienteId());
        acessoResponse.setPortaId(acesso.getId().getPortaId());

        return acessoResponse;
    }
}
