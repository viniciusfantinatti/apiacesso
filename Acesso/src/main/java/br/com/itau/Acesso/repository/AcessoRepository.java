package br.com.itau.Acesso.repository;

import br.com.itau.Acesso.model.Acesso;
import br.com.itau.Acesso.model.AcessoPK;
import org.springframework.data.repository.CrudRepository;

public interface AcessoRepository extends CrudRepository<Acesso, AcessoPK> {
}
