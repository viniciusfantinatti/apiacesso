package br.com.itau.Acesso.client;

import br.com.itau.Acesso.model.Porta;

public class PortaClientFallback implements PortaClient {

    @Override
    public Porta buscarPortaPorId(Integer id) {
        Porta portaFallback = new Porta(999, "99","99B99");

        return portaFallback;
    }
}
