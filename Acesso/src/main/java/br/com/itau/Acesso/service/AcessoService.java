package br.com.itau.Acesso.service;

import br.com.itau.Acesso.producer.LogAcesso;
import br.com.itau.Acesso.client.ClienteClients;
import br.com.itau.Acesso.client.PortaClient;
import br.com.itau.Acesso.exceptions.AcessoNotFoundException;
import br.com.itau.Acesso.model.Acesso;
import br.com.itau.Acesso.model.AcessoPK;
import br.com.itau.Acesso.model.Cliente;
import br.com.itau.Acesso.model.Porta;
import br.com.itau.Acesso.producer.AcessoProducer;
import br.com.itau.Acesso.repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private ClienteClients clienteClients;

    @Autowired
    private PortaClient portaClient;

    @Autowired
    private AcessoProducer acessoProducer;


    public Acesso criarAcesso(Acesso acesso){
        Cliente objCliente = clienteClients.buscarClientePorId(acesso.getId().getClienteId());
        Porta objPorta = portaClient.buscarPortaPorId(acesso.getId().getPortaId());

        AcessoPK acessoPK = new AcessoPK();
        acessoPK.setClienteId(objCliente.getId());
        acessoPK.setPortaId(objPorta.getIdPorta());

        acesso.setId(acessoPK);
        return acessoRepository.save(acesso);
    }

    public void deletarAcesso(AcessoPK acessoPK){

        acessoRepository.deleteById(acessoPK);
    }

    public Acesso buscarAcessoPorIdClienteIdPorta(AcessoPK acessoPK){
        Cliente objCliente = clienteClients.buscarClientePorId(acessoPK.getClienteId());
        Porta objPorta = portaClient.buscarPortaPorId(acessoPK.getPortaId());

        LogAcesso logAcesso = new LogAcesso();
        logAcesso.setNomeCliente(objCliente.getNome());
        logAcesso.setAndar(objPorta.getAndar());
        logAcesso.setSala(objPorta.getSala());
        logAcesso.setDataAcesso(LocalDateTime.now());

        Optional<Acesso> optAcesso = acessoRepository.findById(acessoPK);

        if (!optAcesso.isPresent()){
            logAcesso.setTemAcesso(false);
            acessoProducer.enviarMsgKafka(logAcesso);

            throw new AcessoNotFoundException();
        }
        logAcesso.setTemAcesso(true);
        acessoProducer.enviarMsgKafka(logAcesso);

        return optAcesso.get();
    }
}
