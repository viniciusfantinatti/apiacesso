package br.com.itau.Acesso.client;

import br.com.itau.Acesso.model.Cliente;

public class ClienteClientFallback implements ClienteClients {
    @Override
    public Cliente buscarClientePorId(Integer id) {
        Cliente clienteFallBack = new Cliente(999, "TESTE-FALLBACK");

        return clienteFallBack;

    }
}
