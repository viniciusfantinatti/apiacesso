package br.com.itau.Acesso.client;

import br.com.itau.Acesso.model.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente", configuration = ClienteClientConfiguration.class)
public interface ClienteClients {

    @GetMapping("/cliente/{id}")
    Cliente buscarClientePorId(@PathVariable Integer id);
}

