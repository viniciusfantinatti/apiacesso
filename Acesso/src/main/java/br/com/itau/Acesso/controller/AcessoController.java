package br.com.itau.Acesso.controller;

import br.com.itau.Acesso.DTO.AcessoMapper;
import br.com.itau.Acesso.DTO.AcessoRequestResponse;
import br.com.itau.Acesso.model.Acesso;
import br.com.itau.Acesso.model.AcessoPK;
import br.com.itau.Acesso.repository.AcessoRepository;
import br.com.itau.Acesso.service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @Autowired
    private AcessoMapper mapper;


    //INCLUSAO
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AcessoRequestResponse criarAcesso(@RequestBody AcessoPK acessoPK){
        Acesso objAcesso = mapper.converterParaAcesso(acessoPK.getClienteId(), acessoPK.getPortaId());

        return mapper.converterParaAcessoRequestResponse(acessoService.criarAcesso(objAcesso));
    }

    //DELETE
    @DeleteMapping("/{clienteId}/{portaId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void excluirAcesso(@PathVariable Integer clienteId, @PathVariable Integer portaId){
        AcessoPK acessoPK = mapper.converterParaAcessoPK(clienteId, portaId);

        acessoService.deletarAcesso(acessoPK);
    }

    //CONSULTA
    @GetMapping("/{clienteId}/{portaId}")
    @ResponseStatus(HttpStatus.OK)
    public AcessoRequestResponse buscarAcessoPorIdClienteIdPorta(@PathVariable Integer clienteId, @PathVariable Integer portaId){
        AcessoPK acessoPK = mapper.converterParaAcessoPK(clienteId, portaId);

        return mapper.converterParaAcessoRequestResponse(acessoService.buscarAcessoPorIdClienteIdPorta(acessoPK));
    }
}
