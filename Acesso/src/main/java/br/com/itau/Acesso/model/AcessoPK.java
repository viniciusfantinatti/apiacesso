package br.com.itau.Acesso.model;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class AcessoPK implements Serializable {

    private Integer portaId;
    private Integer clienteId;

    public Integer getPortaId() {
        return portaId;
    }

    public void setPortaId(Integer portaId) {
        this.portaId = portaId;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }
}
