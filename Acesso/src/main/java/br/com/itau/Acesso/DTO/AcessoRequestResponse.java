package br.com.itau.Acesso.DTO;

public class AcessoRequestResponse {

    private Integer portaId;
    private Integer clienteId;

    public AcessoRequestResponse() {
    }

    public AcessoRequestResponse(Integer portaId, Integer clienteId) {
        this.portaId = portaId;
        this.clienteId = clienteId;
    }

    public Integer getPortaId() {
        return portaId;
    }

    public void setPortaId(Integer portaId) {
        this.portaId = portaId;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }
}
