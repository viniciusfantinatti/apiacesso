package br.com.itau.Acesso.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table
public class Acesso implements Serializable {

    @EmbeddedId
    private AcessoPK id;

    public Acesso() {
    }

    public Acesso(AcessoPK id) {
        this.id = id;
    }

    public AcessoPK getId() {
        return id;
    }

    public void setId(AcessoPK id) {
        this.id = id;
    }
}
