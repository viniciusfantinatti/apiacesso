package br.com.itau.Acesso.client.decoder;

import br.com.itau.Acesso.exceptions.PortaInvalidException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class PortaClientDecoder implements ErrorDecoder {

    private ErrorDecoder decoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404){
            throw new PortaInvalidException();
        }else{
            return decoder.decode(s, response);
        }
    }
}
