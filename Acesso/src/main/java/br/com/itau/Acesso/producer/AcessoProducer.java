package br.com.itau.Acesso.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AcessoProducer {

    @Autowired
    private KafkaTemplate<String, LogAcesso> kafkaProducer;

    public void enviarMsgKafka(LogAcesso logAcesso){
        kafkaProducer.send("spec3-vinicius-fantinatti-1", logAcesso);
    }
}
