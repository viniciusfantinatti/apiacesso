package br.com.itau.Acesso.model;

public class Porta {

    private Integer idPorta;
    private String andar;
    private String sala;

    public Porta() {
    }

    public Porta(Integer idPorta, String andar, String sala) {
        this.idPorta = idPorta;
        this.andar = andar;
        this.sala = sala;
    }

    public Integer getIdPorta() {
        return idPorta;
    }

    public void setIdPorta(Integer idPorta) {
        this.idPorta = idPorta;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
}
