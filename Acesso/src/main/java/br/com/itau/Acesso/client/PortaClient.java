package br.com.itau.Acesso.client;

import br.com.itau.Acesso.model.Porta;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name = "porta", configuration = PortaClientConfiguration.class)
public interface PortaClient {

    @GetMapping("/porta/{id}")
    Porta buscarPortaPorId(@PathVariable Integer id);
}
