package br.com.itau.Acesso.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Cliente INVALIDO")
public class ClienteInvalidException extends RuntimeException {
}
