package br.com.itau.Acesso.producer;

import java.time.LocalDateTime;

public class LogAcesso {

    private String nomeCliente;
    private String andar;
    private String sala;
    private Boolean temAcesso;
    private LocalDateTime dataAcesso;

    public LogAcesso() {
    }

    public LogAcesso(String nomeCliente, String andar, String sala, Boolean temAcesso, LocalDateTime dataAcesso) {
        this.nomeCliente = nomeCliente;
        this.andar = andar;
        this.sala = sala;
        this.temAcesso = temAcesso;
        this.dataAcesso = dataAcesso;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public Boolean getTemAcesso() {
        return temAcesso;
    }

    public void setTemAcesso(Boolean temAcesso) {
        this.temAcesso = temAcesso;
    }

    public LocalDateTime getDataAcesso() {
        return dataAcesso;
    }

    public void setDataAcesso(LocalDateTime dataAcesso) {
        this.dataAcesso = dataAcesso;
    }
}