package br.com.itau.Acesso.consumer;

import br.com.itau.Acesso.producer.LogAcesso;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;

@Component
public class LogAcessoConsumer {

    @KafkaListener(topics = "spec3-vinicius-fantinatti-1", groupId = "SistemaGG")
    public void receber(@Payload LogAcesso logAcesso){
        gerarArquivoLog("/home/a2w/Workspace/Vinicius/Especializacao/log.txt", logAcesso);
        System.out.println("LOG: " + logAcesso.getNomeCliente() + " | " + logAcesso.getAndar() + " | " +
                logAcesso.getSala() + " | " + logAcesso.getTemAcesso() + " | " + logAcesso.getDataAcesso());
    }

    private static void gerarArquivoLog(String nomeArq, LogAcesso logAcesso){
        try{
            FileWriter writer = new FileWriter(nomeArq, true);

            writer.write(logAcesso.getNomeCliente());
            writer.write(';');
            writer.write(logAcesso.getAndar());
            writer.write(';');
            writer.write(logAcesso.getSala());
            writer.write(';');
            writer.write(logAcesso.getTemAcesso().toString());
            writer.write(';');
            writer.write(logAcesso.getDataAcesso().toString());
            writer.write('\n');

            writer.flush();
            writer.close();


        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
