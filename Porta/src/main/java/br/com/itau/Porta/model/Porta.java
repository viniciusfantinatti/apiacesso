package br.com.itau.Porta.model;

import javax.persistence.*;

@Entity
@Table
public class Porta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 5)
    private Integer idPorta;

    @Column(length = 2)
    private String andar;

    @Column(length = 5)
    private String sala;

    public Porta() {
    }

    public Porta(Integer idPorta, String andar, String sala) {
        this.idPorta = idPorta;
        this.andar = andar;
        this.sala = sala;
    }

    public Integer getIdPorta() {
        return idPorta;
    }

    public void setIdPorta(Integer idPorta) {
        this.idPorta = idPorta;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
}
