package br.com.itau.Porta.service;

import br.com.itau.Porta.exception.PortaNotFoundException;
import br.com.itau.Porta.model.Porta;
import br.com.itau.Porta.repository.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta criarPorta(Porta porta){

        return portaRepository.save(porta);
    }

    public Porta buscarPortaPorId(Integer id){
        Optional<Porta> optPorta = portaRepository.findById(id);

        if (!optPorta.isPresent()){
            throw new PortaNotFoundException();
        }

        return optPorta.get();
    }
}
