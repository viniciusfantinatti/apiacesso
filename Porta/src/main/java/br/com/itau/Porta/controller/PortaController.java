package br.com.itau.Porta.controller;

import br.com.itau.Porta.model.Porta;
import br.com.itau.Porta.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    //INCLUSÃO
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta criarPorta(@RequestBody Porta porta){

        return portaService.criarPorta(porta);
    }

    //CONSULTA
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Porta buscarPortaPorId(@PathVariable Integer id){

        return portaService.buscarPortaPorId(id);
    }
}
