package br.com.itau.Porta.repository;

import br.com.itau.Porta.model.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository<Porta, Integer> {
}
